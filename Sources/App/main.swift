import Foundation
import Vapor

let drop = Droplet()

// MARK: Home
drop.get("/") { request in
    return try drop.view.make("home")
}

// MARK: Login
let loginController = LoginController()
drop.post("login", handler: loginController.login)

// MARK: Register
let registerController = RegisterController()
drop.post("register", handler: registerController.register)

drop.run()


// MARK: Handling different environments
/*
func helloResponse(forEnvironment environment: Environment) -> String {
    print(drop.workDir)
    switch environment {
    case .development: return "Bonzy is under development"
    case .test: return "Bonzy is under testing"
    case .production: return "Bonzy is in production"
    default: return "Bonzy is gonna kick your asses"
    }
}
*/
