//
//  Credential.swift
//  FirstSteps
//
//  Created by Rajiv Jhoomuck on 25/03/2017.
//
//

import Foundation
import Vapor
import HTTP

struct Credential {
    let username: Valid<OnlyAlphanumeric>
    let password: Valid<OnlyAlphanumeric>
    
    init(from request: Request) throws {
        username = try request.data["username"].validated()
        password = try request.data["password"].validated()
    }
}
