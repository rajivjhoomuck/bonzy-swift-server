//
//  RegistrationForm.swift
//  FirstSteps
//
//  Created by Rajiv Jhoomuck on 25/03/2017.
//
//

import Foundation
import HTTP
import Vapor

struct RegistrationForm {
    let email: Valid<Email>
    let username: Valid<OnlyAlphanumeric>
    let password: Valid<OnlyAlphanumeric>
    
    init(from request: Request) throws {
        username = try request.data["username"].validated()
        email = try request.data["email"].validated()
        password = try request.data["password"].validated()
    }
}
