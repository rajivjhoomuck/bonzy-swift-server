//
//  RegisterController.swift
//  FirstSteps
//
//  Created by Rajiv Jhoomuck on 25/03/2017.
//
//

import Foundation
import HTTP
import Vapor

struct RegisterController {
    
    func register(request: Request) throws -> ResponseRepresentable {
        if let registrationForm = try? RegistrationForm(from: request) {
            let message = "Thank you \(registrationForm.username)for registering with this awesome service. Please verify your email address (\(registrationForm.email)) to proceed with registration."
            
            return JSON(["message": message.makeNode()])
        }
        else {
            throw Abort.custom(status: .badRequest, message: "There was a problem processing your registration request. Please verify the information that you send.")
        }
    }
}
