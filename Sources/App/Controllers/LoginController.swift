//
//  LoginController.swift
//  FirstSteps
//
//  Created by Rajiv Jhoomuck on 25/03/2017.
//
//

import Foundation
import HTTP
import Vapor

struct LoginController {
    
    func login(request: Request) throws -> ResponseRepresentable {
        // TODO: Add better handling of the login process
        
        if let _ = try? Credential(from: request) {
            return try drop.view.make("dashboard")
        } else {
            throw Abort.custom(status: Status.badRequest, message: "Could not proceeed login with the credential details that you provided")
        }
        
    }
}
